#include <iostream>
#include <algorithm>
#include <string>
#include <sstream>
#include <vector>
#include <chrono>
#include <cstdlib>
#include <ctime>
#include "TestCases.hpp"

std::string collapseSpaces(std::string s);
std::vector<std::string> split(std::string s);
bool isPalindrome(std::string word, int start, int end);
bool isWordSymmetric(const std::vector<std::string>& words, int start, int end);
long vectorSum(const std::vector<int>& data, unsigned int position);
int vectorMin(const std::vector<int>& data, unsigned int position);
void quickSort(std::vector<int>& data, int start, int end);

void selectionSort(std::vector<int>& data, int start, int end);

int main()
{
	TestCases::runTestCases();
}

// ------------------------------------------------------------------
//
// Provided code to remove spaces from a string (and capitalize it)
//
// ------------------------------------------------------------------
std::string collapseSpaces(std::string s)
{
	s.erase(std::remove_if(s.begin(), s.end(), isspace), s.end());
	std::transform(s.begin(), s.end(), s.begin(), toupper);	// Capitalize all
	return s;
}

// ------------------------------------------------------------------
//
// Solution string split code
//
// ------------------------------------------------------------------
std::vector<std::string> split(std::string s)
{
	std::istringstream is(s);
	std::vector<std::string> words;

	do
	{
		std::string word;
		is >> word;
		if (word.length() > 0)
		{
			words.push_back(word);
		}
	} while (is);

	return words;
}

// ------------------------------------------------------------------
//
// Provided quicksort partition code
//
// ------------------------------------------------------------------
int partition(std::vector<int>& data, int start, int end)
{
	int middle = (start + end) / 2;
	std::swap(data[start], data[middle]);
	int pivotIndex = start;
	int pivotValue = data[start];
	for (int scan = start + 1; scan <= end; scan++)
	{
		if (data[scan] < pivotValue)
		{
			pivotIndex++;
			std::swap(data[scan], data[pivotIndex]);
		}
	}

	std::swap(data[pivotIndex], data[start]);

	return pivotIndex;
}

// ------------------------------------------------------------------
//
// Provided quicksort code with edit
//
// ------------------------------------------------------------------
void quickSort(std::vector<int>& data, int start, int end)
{
	if (end - start <= 10)
	{
		selectionSort(data, start, end);
	}
    else if (start < end)
	{
		int pivot = partition(data, start, end);
		quickSort(data, start, pivot - 1);
		quickSort(data, pivot + 1, end);
	}
}

// ------------------------------------------------------------------
//
// Assigned Code
//
// ------------------------------------------------------------------

bool isPalindrome(std::string word, int start, int end)
{
	if (start == end)
	{
		return true;
	}
	else if (word[start] != word[end])
	{
		return false;
	}
	else if (word[start] == word[end] && start - 1 == end)
	{
		return true;
	}
	else if (word[start] == word[end])
	{
		return isPalindrome(word, start + 1, end -1);
	}
	return false;  // function should not ever reach this point
}

bool isWordSymmetric(const std::vector<std::string>& words, int start, int end)
{
	if (start == end)
	{
		return true;
	}
	else if (words[start] != words[end])
	{
		return false;
	}
	else if (words[start] == words[end] && start - 1 == end)
	{
		return true;
	}
	else if (words[start] == words[end])
	{
		return isWordSymmetric(words, start + 1, end -1);
	}
	return false;  // function should not ever reach this point
}

long vectorSum(const std::vector<int>& data, unsigned int position)
{
	if (position == data.size())
	{
		return 0;
	}
	else
	{
		return data[position] + vectorSum(data, position + 1);
	}
}

int vectorMin(const std::vector<int>& data, unsigned int position)
{
	if (position == data.size() - 1)
	{
		return data[position];
	}
	else
	{
		int compare = vectorMin(data, position + 1);
		if (data[position] < compare)
        {
            return data[position];
        }
        else
        {
            return compare;
        }
	}
}

void selectionSort(std::vector<int>& data, int start, int end)
{
	for (int begin = start; begin < end; begin++)
	{
		int minPos = begin;
		for (int scan = begin + 1; scan <= end; scan++)
		{
			if (data[scan] < data[minPos])
			{
				minPos = scan;
			}
		}
		if (begin != minPos)
		{
			std::swap(data[begin], data[minPos]);
		}
	}
}